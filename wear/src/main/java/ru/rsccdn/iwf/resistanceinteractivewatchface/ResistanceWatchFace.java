package ru.rsccdn.iwf.resistanceinteractivewatchface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.icu.text.SimpleDateFormat;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.ColorInt;
import android.support.v7.graphics.Palette;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.text.TextPaint;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.SurfaceHolder;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class ResistanceWatchFace extends CanvasWatchFaceService {

    /*
     * Updates rate in milliseconds for interactive mode. We update once a second to advance the
     * second hand.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);

    private static final int MSG_UPDATE_TIME = 0;

    @Override
    public Engine onCreateEngine() {
        return new Engine();
    }

    //Battery details
    private int batLevel;
    private boolean isCharging;

    private void updateBatt() {

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, ifilter);

        //Level
        batLevel = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING || status == BatteryManager.BATTERY_STATUS_FULL;
    }



    private class Engine extends CanvasWatchFaceService.Engine {

        Typeface tf;

        private static final float HOUR_STROKE_WIDTH = 5f;
        private static final float MINUTE_STROKE_WIDTH = 3f;
        private static final float SECOND_TICK_STROKE_WIDTH = 2f;

        private static final float BATT_TICK_STROKE_WIDTH = 2f;

        private static final float CENTER_GAP_AND_CIRCLE_RADIUS = 4f;

        private static final int SHADOW_RADIUS = 6;

        private Calendar mCalendar;
        private boolean mRegisteredTimeZoneReceiver = false;
        private boolean mMuteMode;

        private float mCenterX;
        private float mCenterY;

        private float mSecondHandLength;
        private float sMinuteHandLength;
        private float sHourHandLength;

        private int mWatchHandColor;
        private int mWatchHandHighlightColor;
        private int mWatchHandShadowColor;

        private Paint mHourPaint;
        private Paint mMinutePaint;
        private Paint mSecondPaint;
        private Paint mTickAndCirclePaint;

        private Paint mBackgroundPaint;
        private Bitmap mBackgroundBitmap;
        private Bitmap mBackgroundAmbBitmap;
        private Bitmap mGrayBackgroundBitmap;

        private boolean mAmbient;
        private boolean mLowBitAmbient;
        private boolean mBurnInProtection;
        private boolean mIsRound;

        private java.text.DateFormat dateFormat;
        private SimpleDateFormat dowFormat;
        private TextPaint datePaint;

        private TextPaint septicyclesPaint;

        private Paint mWatchBattCirclePaint;

        private final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };

        /* Handler to update the time once a second in interactive mode. */
        private final Handler mUpdateTimeHandler = new EngineHandler(this);

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            tf = Typeface.createFromAsset(getAssets(), "exo2bold.ttf");

            setWatchFaceStyle(new WatchFaceStyle.Builder(ResistanceWatchFace.this)
                    .setAcceptsTapEvents(true)
                    .setShowUnreadCountIndicator(true)
                    .setStatusBarGravity(Gravity.CENTER_HORIZONTAL | Gravity.TOP)
                    .build());

            mCalendar = Calendar.getInstance();
            mIsRound = getApplicationContext().getResources().getConfiguration().isScreenRound();

            initializeBackground();
            initializeWatchFace();
        }


        private void initializeBackground() {
            mBackgroundPaint = new Paint();
            mBackgroundPaint.setColor(Color.BLACK);
            if (mIsRound) {
                mBackgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_res_1);
            } else {
                mBackgroundBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_res_1_rect2);
            }
            mBackgroundAmbBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_res_1_a_blue);

            /* Extracts colors from background image to improve watchface style. */
            Palette.from(mBackgroundBitmap).generate(new Palette.PaletteAsyncListener() {
                @Override
                public void onGenerated(Palette palette) {
                    if (palette != null) {
                        mWatchHandHighlightColor = palette.getVibrantColor(Color.WHITE);
                        mWatchHandColor = palette.getLightVibrantColor(Color.WHITE);
                        mWatchHandShadowColor = palette.getDarkMutedColor(Color.BLACK);
                        updateWatchHandStyle();
                    }
                }
            });
        }

        private void initializeWatchFace() {
            /* Set defaults for colors */
            mWatchHandColor = Color.WHITE;
            mWatchHandHighlightColor = Color.WHITE;
            mWatchHandShadowColor = Color.BLACK;

            mHourPaint = new Paint();
            mHourPaint.setColor(mWatchHandColor);
            mHourPaint.setStrokeWidth(HOUR_STROKE_WIDTH);
            mHourPaint.setAntiAlias(true);
            mHourPaint.setStrokeCap(Paint.Cap.ROUND);
            mHourPaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);

            mMinutePaint = new Paint();
            mMinutePaint.setColor(mWatchHandColor);
            mMinutePaint.setStrokeWidth(MINUTE_STROKE_WIDTH);
            mMinutePaint.setAntiAlias(true);
            mMinutePaint.setStrokeCap(Paint.Cap.ROUND);
            mMinutePaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);

            mSecondPaint = new Paint();
            mSecondPaint.setColor(mWatchHandHighlightColor);
            mSecondPaint.setStrokeWidth(SECOND_TICK_STROKE_WIDTH);
            mSecondPaint.setAntiAlias(true);
            mSecondPaint.setStrokeCap(Paint.Cap.ROUND);
            mSecondPaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);

            mTickAndCirclePaint = new Paint();
            mTickAndCirclePaint.setColor(mWatchHandColor);
            mTickAndCirclePaint.setStrokeWidth(SECOND_TICK_STROKE_WIDTH);
            mTickAndCirclePaint.setAntiAlias(true);
            mTickAndCirclePaint.setStyle(Paint.Style.STROKE);
            mTickAndCirclePaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);

            mWatchBattCirclePaint = new Paint();
            mWatchBattCirclePaint.setColor(getBattColor());
            mWatchBattCirclePaint.setStrokeWidth(BATT_TICK_STROKE_WIDTH);
            mWatchBattCirclePaint.setAntiAlias(true);
            mWatchBattCirclePaint.setStyle(Paint.Style.STROKE);
            mWatchBattCirclePaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);

            dowFormat = new SimpleDateFormat("EEEE", Locale.getDefault());
//            dateFormat = new SimpleDateFormat("dd MMMM", Locale.getDefault());
            dateFormat = android.text.format.DateFormat.getLongDateFormat(getApplicationContext());
            datePaint = new TextPaint();
            datePaint.setColor(mWatchHandColor);
            datePaint.setAntiAlias(true);
            datePaint.setTypeface(tf);
            datePaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10, getResources().getDisplayMetrics()));

            septicyclesPaint = new TextPaint();
            septicyclesPaint.setColor(mWatchHandColor);
            septicyclesPaint.setAntiAlias(true);
            septicyclesPaint.setTypeface(tf);
            septicyclesPaint.setTextSize(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));

        }

        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION, false);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            mAmbient = inAmbientMode;

            updateWatchHandStyle();
            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer();
        }

        private void updateWatchHandStyle(){
            if (mAmbient){
                int mainColor = Color.WHITE;
                if (mBurnInProtection || mLowBitAmbient) {
                    mainColor = Color.BLUE;
                }
                mHourPaint.setColor(mainColor);
                mMinutePaint.setColor(mainColor);
                mSecondPaint.setColor(mainColor);
                mTickAndCirclePaint.setColor(mainColor);
                mWatchBattCirclePaint.setColor(getBattColor());
                datePaint.setColor(mainColor);
                septicyclesPaint.setColor(mainColor);

                mHourPaint.setAntiAlias(false);
                mMinutePaint.setAntiAlias(false);
                mSecondPaint.setAntiAlias(false);
                mTickAndCirclePaint.setAntiAlias(false);
                mWatchBattCirclePaint.setAntiAlias(false);
                datePaint.setAntiAlias(false);
                septicyclesPaint.setAntiAlias(false);

                mHourPaint.clearShadowLayer();
                mMinutePaint.clearShadowLayer();
                mSecondPaint.clearShadowLayer();
                mTickAndCirclePaint.clearShadowLayer();
                mWatchBattCirclePaint.clearShadowLayer();

            } else {
                mHourPaint.setColor(mWatchHandColor);
                mMinutePaint.setColor(mWatchHandColor);
                mSecondPaint.setColor(mWatchHandHighlightColor);
                mTickAndCirclePaint.setColor(mWatchHandColor);
                mWatchBattCirclePaint.setColor(getBattColor());
                datePaint.setColor(mWatchHandColor);
                septicyclesPaint.setColor(mWatchHandColor);

                mHourPaint.setAntiAlias(true);
                mMinutePaint.setAntiAlias(true);
                mSecondPaint.setAntiAlias(true);
                mTickAndCirclePaint.setAntiAlias(true);
                mWatchBattCirclePaint.setAntiAlias(true);
                datePaint.setAntiAlias(true);
                septicyclesPaint.setAntiAlias(true);

                mHourPaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);
                mMinutePaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);
                mSecondPaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);
                mTickAndCirclePaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);
                mWatchBattCirclePaint.setShadowLayer(SHADOW_RADIUS, 0, 0, mWatchHandShadowColor);
            }
        }

        @Override
        public void onInterruptionFilterChanged(int interruptionFilter) {
            super.onInterruptionFilterChanged(interruptionFilter);
            boolean inMuteMode = (interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE);

            /* Dim display in mute mode. */
            if (mMuteMode != inMuteMode) {
                mMuteMode = inMuteMode;
                mHourPaint.setAlpha(inMuteMode ? 100 : 255);
                mMinutePaint.setAlpha(inMuteMode ? 100 : 255);
                mSecondPaint.setAlpha(inMuteMode ? 80 : 255);
                invalidate();
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);

            /*
             * Find the coordinates of the center point on the screen, and ignore the window
             * insets, so that, on round watches with a "chin", the watch face is centered on the
             * entire screen, not just the usable portion.
             */
            mCenterX = width / 2f;
            mCenterY = height / 2f;

            /*
             * Calculate lengths of different hands based on watch screen size.
             */
            mSecondHandLength = (float) (mCenterX * 0.875);
            sMinuteHandLength = (float) (mCenterX * 0.75);
            sHourHandLength = (float) (mCenterX * 0.5);


            /* Scale loaded background image (more efficient) if surface dimensions change. */
            float scale = ((float) width) / (float) mBackgroundBitmap.getWidth();

            mBackgroundBitmap = Bitmap.createScaledBitmap(mBackgroundBitmap,
                    (int) (mBackgroundBitmap.getWidth() * scale),
                    (int) (mBackgroundBitmap.getHeight() * scale), true);

            mBackgroundAmbBitmap = Bitmap.createScaledBitmap(mBackgroundAmbBitmap,
                    (int) (mBackgroundAmbBitmap.getWidth() * scale),
                    (int) (mBackgroundAmbBitmap.getHeight() * scale), true);

            /*
             * Create a gray version of the image only if it will look nice on the device in
             * ambient mode. That means we don't want devices that support burn-in
             * protection (slight movements in pixels, not great for images going all the way to
             * edges) and low ambient mode (degrades image quality).
             *
             * Also, if your watch face will know about all images ahead of time (users aren't
             * selecting their own photos for the watch face), it will be more
             * efficient to create a black/white version (png, etc.) and load that when you need it.
             */
            if (!mBurnInProtection && !mLowBitAmbient) {
                initGrayBackgroundBitmap();
            } else {
                initGrayAmbBackgroundBitmap();
            }
        }

        private void initGrayBackgroundBitmap() {
            mGrayBackgroundBitmap = Bitmap.createBitmap(
                    mBackgroundBitmap.getWidth(),
                    mBackgroundBitmap.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mGrayBackgroundBitmap);
            Paint grayPaint = new Paint();
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
            grayPaint.setColorFilter(filter);
            canvas.drawBitmap(mBackgroundBitmap, 0, 0, grayPaint);
        }

        private void initGrayAmbBackgroundBitmap() {
            mBackgroundAmbBitmap = Bitmap.createBitmap(
                    mBackgroundAmbBitmap.getWidth(),
                    mBackgroundAmbBitmap.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(mBackgroundAmbBitmap);
            Paint grayPaint = new Paint();
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(colorMatrix);
            grayPaint.setColorFilter(filter);
            canvas.drawBitmap(mBackgroundAmbBitmap, 0, 0, grayPaint);
        }
        /**
         * Captures tap event (and tap type). The {@link WatchFaceService#TAP_TYPE_TAP} case can be
         * used for implementing specific logic to handle the gesture.
         */
        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    // The user has started touching the screen.
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                    // The user has started a different gesture or otherwise cancelled the tap.
                    break;
                case TAP_TYPE_TAP:
                    // The user has completed the tap gesture.
                    Intent farmTimer = new Intent(getApplicationContext(), FarmTimer.class);
                    farmTimer.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(farmTimer);
                    break;
            }
            invalidate();
        }
        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            long now = System.currentTimeMillis();
            mCalendar.setTimeInMillis(now);

            drawBackground(canvas);
            drawWatchFace(canvas);

            String dateText = dateFormat.format(now);
            String dowText = dowFormat.format(now);
            Rect dateBounds = new Rect();
            Rect dowBounds = new Rect();
            int dateX;
            int dateY;
            int dowX;
            int dowY;

            datePaint.getTextBounds(dowText, 0, dowText.length(), dowBounds);
            dowX = Math.abs(bounds.centerX() - dowBounds.centerX());
            dowY = bounds.centerY() + bounds.centerY()/2  + dowBounds.height();
            canvas.drawText(dowText, dowX, dowY, datePaint);

            datePaint.getTextBounds(dateText, 0, dateText.length(), dateBounds);
            dateX = Math.abs(bounds.centerX() - dateBounds.centerX());
            dateY = Math.round(bounds.centerY() + bounds.centerY()/2  + dateBounds.height() * 1.2f + dowBounds.height());
            canvas.drawText(dateText, dateX, dateY, datePaint);

            drawBatt(canvas);

            drawSepticycles(canvas, bounds);
        }

        private void drawSepticycles(Canvas canvas, Rect bounds) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss Z");
            SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long startTime = 0;
            try {
                startTime = format.parse("2018/10/16 20:00:00 +0000").getTime();
            } catch (Exception e) {}
            if (startTime > 0) {
                long diff = System.currentTimeMillis() - startTime;
                long diffCC = diff / (175 * 60 * 60 * 1000);
                long startThisSC = diffCC * 175 * 60 * 60 * 1000 + startTime;
                long diffCP = (System.currentTimeMillis() - startThisSC) / (5 * 60 * 60 * 1000);
                long startThisCP = diffCP * 5 * 60 * 60 * 1000 + startThisSC;

                long endSC = startThisSC + 175 * 60 * 60 * 1000;
                long diffEndSC = endSC - System.currentTimeMillis();
                int endSCHours = (int) ((diffEndSC) / (1000*60*60));
                int endSCMin = (int) (diffEndSC - (1000*60*60*endSCHours)) / (1000*60);
                int endSCSec = (int) (diffEndSC - (1000*60*60*endSCHours) - (1000*60*endSCMin)) / 1000;

                long endCP = startThisCP + 5 * 60 * 60 * 1000;
                long diffEndCP = endCP - System.currentTimeMillis();
                int endCPHours = (int) ((diffEndCP) / (1000*60*60));
                int endCPMin = (int) (diffEndCP - (1000*60*60*endCPHours)) / (1000*60);
                int endCPSec = (int) (diffEndCP - (1000*60*60*endCPHours) - (1000*60*endCPMin)) / 1000;


                Rect scBounds = new Rect();
                Rect cpBounds = new Rect();
                int scX;
                int scY;
                int cpX;
                int cpY;

                String scText = ""+String.format("%3s", endSCHours).replace(' ', '0')+":"+String.format("%2s", endSCMin).replace(' ', '0')+":"+String.format("%2s", endSCSec).replace(' ', '0');
                String cpText = ""+String.format("%1s", endCPHours).replace(' ', '0')+":"+String.format("%2s", endCPMin).replace(' ', '0')+":"+String.format("%2s", endCPSec).replace(' ', '0');
                if (mAmbient) {
                    scText = ""+String.format("%3s", endSCHours).replace(' ', '0')+":"+String.format("%2s", endSCMin).replace(' ', '0');
                    cpText = ""+String.format("%1s", endCPHours).replace(' ', '0')+":"+String.format("%2s", endCPMin).replace(' ', '0');
                }

                if (mAmbient) {
                    septicyclesPaint.getTextBounds("000:00", 0, "000:00".length(), cpBounds);
                } else {
                    septicyclesPaint.getTextBounds("000:00:00", 0, "000:00:00".length(), cpBounds);
                }
                cpX = Math.abs(bounds.centerX() - cpBounds.centerX());
                cpY = Math.round(bounds.centerY()/2 * 0.6f  + cpBounds.height());
                canvas.drawText(scText, cpX, cpY, septicyclesPaint);

                if (mAmbient) {
                    septicyclesPaint.getTextBounds("0:00", 0, "0:00".length(), scBounds);
                } else {
                    septicyclesPaint.getTextBounds("0:00:00", 0, "0:00:00".length(), scBounds);
                }
                scX = Math.abs(bounds.centerX() - scBounds.centerX());
                scY = Math.round(bounds.centerY()/2  * 0.6f + scBounds.height() * 1.2f + cpBounds.height());
                canvas.drawText(cpText, scX, scY, septicyclesPaint);
            }
        }

        private void drawBatt(Canvas canvas) {

            updateBatt();
            if (batLevel > 0) {
                mWatchBattCirclePaint.setColor(getBattColor());
                if (mIsRound) {
                    int ticksCntMax = 350;
                    int ticksCntCurr = Math.round(batLevel * ticksCntMax / 100);
                    drawCircleTicks(canvas, mWatchBattCirclePaint, ticksCntMax, 8, 22, ticksCntCurr);
                } else {
                    int ticksCntMax = 300;
                    int ticksCntCurr = Math.round(batLevel * ticksCntMax / 100);
                    drawRectTicks(canvas, mWatchBattCirclePaint, ticksCntMax, 10, 22, ticksCntCurr);
                }

            }
        }

        private int getBattColor() {
            int color = Color.BLUE;
            if (mAmbient){
                if (!mBurnInProtection && !mLowBitAmbient) {
                    color = Color.WHITE;
                }
                if (isCharging) {
                    color = Color.GREEN;
                } else {
                    if (batLevel < 15) {
                        color = Color.RED;
                    } else if (batLevel < 30) {
                        color = Color.YELLOW;
                    }
                }
            } else {
                color = Color.argb(255, 132, 42, 211);
                if (isCharging) {
                    color = Color.argb(255, 1, 119, 46);
                } else {
                    if (batLevel < 15) {
                        color = Color.argb(255, 188, 11, 11);
                    } else if (batLevel < 30) {
                        color = Color.argb(255, 237, 137, 30);
                    }
                }
            }
            return color;
        }

        private void drawBackground(Canvas canvas) {

            if (mAmbient && (mLowBitAmbient || mBurnInProtection)) {
                canvas.drawBitmap(mBackgroundAmbBitmap, 0, 0, mBackgroundPaint);
            } else if (mAmbient) {
                canvas.drawBitmap(mGrayBackgroundBitmap, 0, 0, mBackgroundPaint);
            } else {
                canvas.drawBitmap(mBackgroundBitmap, 0, 0, mBackgroundPaint);
            }
        }

        private void drawWatchFace(Canvas canvas) {

            /*
             * Draw ticks. Usually you will want to bake this directly into the photo, but in
             * cases where you want to allow users to select their own photos, this dynamically
             * creates them on top of the photo.
             */

            if (mIsRound) {
                drawCircleTicks(canvas, mTickAndCirclePaint, 12, 20, 0, 12);
                drawCircleTicks(canvas, mTickAndCirclePaint, 60, 10, 0, 60);
            } else {
                drawRectTicks(canvas, mTickAndCirclePaint, 12, 20, 0, 12);
                drawRectTicks(canvas, mTickAndCirclePaint, 60, 10, 0, 60);
            }

            /*
             * These calculations reflect the rotation in degrees per unit of time, e.g.,
             * 360 / 60 = 6 and 360 / 12 = 30.
             */
            final float seconds =
                    (mCalendar.get(Calendar.SECOND) + mCalendar.get(Calendar.MILLISECOND) / 1000f);
            final float secondsRotation = seconds * 6f;

            final float minutesRotation = mCalendar.get(Calendar.MINUTE) * 6f;

            final float hourHandOffset = mCalendar.get(Calendar.MINUTE) / 2f;
            final float hoursRotation = (mCalendar.get(Calendar.HOUR) * 30) + hourHandOffset;

            /*
             * Save the canvas state before we can begin to rotate it.
             */
            canvas.save();

            canvas.rotate(hoursRotation, mCenterX, mCenterY);
            canvas.drawLine(
                    mCenterX,
                    mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                    mCenterX,
                    mCenterY - sHourHandLength,
                    mHourPaint);

            canvas.rotate(minutesRotation - hoursRotation, mCenterX, mCenterY);
            canvas.drawLine(
                    mCenterX,
                    mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                    mCenterX,
                    mCenterY - sMinuteHandLength,
                    mMinutePaint);

            /*
             * Ensure the "seconds" hand is drawn only when we are in interactive mode.
             * Otherwise, we only update the watch face once a minute.
             */
            if (!mAmbient) {
                canvas.rotate(secondsRotation - minutesRotation, mCenterX, mCenterY);
                canvas.drawLine(
                        mCenterX,
                        mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                        mCenterX,
                        mCenterY - mSecondHandLength,
                        mSecondPaint);

            }

            /* Restore the canvas' original orientation. */
            canvas.restore();


        }

        private void drawCircleTicks(Canvas canvas, Paint paint, int ticksCnt, int ticksLen, int margin, int ticksToDraw) {
            float innerTickRadius = mCenterX - margin - ticksLen;
            float outerTickRadius = mCenterX - margin;
            for (int tickIndex = 0; tickIndex < ticksToDraw; tickIndex++) {
                float tickRot = (float) (tickIndex * Math.PI * 2 / ticksCnt);
                float innerX = (float) Math.sin(tickRot) * innerTickRadius;
                float innerY = (float) -Math.cos(tickRot) * innerTickRadius;
                float outerX = (float) Math.sin(tickRot) * outerTickRadius;
                float outerY = (float) -Math.cos(tickRot) * outerTickRadius;
                canvas.drawLine(mCenterX + innerX, mCenterY + innerY,
                        mCenterX + outerX, mCenterY + outerY, paint);
            }
        }

        private void drawRectTicks(Canvas canvas, Paint paint, int ticksCnt, int ticksLen, int margin, int ticksToDraw) {
            float cx = mCenterX;
            float cy = mCenterY;
            float left = cx - mCenterX + margin;
            float top = cy - mCenterY + margin;
            float right = cx + mCenterX - margin;
            float bottom = cy + mCenterY - margin;
            float ry = 0;
            float rx = 0;
            float width = right-left;
            float height = bottom-top;
            Path path = new Path();
            path.moveTo(cx, top + ry);
            path.rLineTo(-width/2, 0);
            path.rQuadTo(-rx, 0, -rx, ry);
            path.rLineTo(0, (height - (2 * ry)));
            path.rQuadTo(0, ry, rx, ry);
            path.rLineTo((width - (2 * rx)), 0);
            path.rQuadTo(rx, 0, rx, -ry);
            path.rLineTo(0, -(height - (2 * ry)));
            path.rQuadTo(0, -ry, -rx, -ry);
            path.rLineTo(-width/2, 0);
            path.close();

            PathMeasure pathMeasure = new PathMeasure();
            pathMeasure.setPath(path,true);
            float length = pathMeasure.getLength();
            float[] pos = new float[2];
            float r = ticksLen;
            for (int i = 0; i < ticksToDraw; i++) {
                pathMeasure.getPosTan(i * (length/ticksCnt),pos,null);
                double angle = Math.atan2(cy - pos[1], cx - pos[0]); //yes, y then x.
                double cos = Math.cos(angle);
                double sin = Math.sin(angle);
                canvas.drawLine(pos[0], pos[1], (float)(pos[0] + cos * r), (float)(pos[1] + sin * r), paint);
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();
                /* Update time zone in case it changed while we weren't visible. */
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            } else {
                unregisterReceiver();
            }

            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            ResistanceWatchFace.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            ResistanceWatchFace.this.unregisterReceiver(mTimeZoneReceiver);
        }

        /**
         * Starts/stops the {@link #mUpdateTimeHandler} timer based on the state of the watch face.
         */
        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer
         * should only run in active mode.
         */
        private boolean shouldTimerBeRunning() {
            return isVisible() && !mAmbient;
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        private void handleUpdateTimeMessage() {
            invalidate();
            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS
                        - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
            }
        }
    }

    private static class EngineHandler extends Handler {
        private final WeakReference<ResistanceWatchFace.Engine> mWeakReference;

        public EngineHandler(ResistanceWatchFace.Engine reference) {
            mWeakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            ResistanceWatchFace.Engine engine = mWeakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }
}
