package ru.rsccdn.iwf.resistanceinteractivewatchface;

class ModSelectItem {
    static final int MOD_NONE = 0;
    static final int MOD_HEATSINK = 1;
    static final int MOD_MULTIHACK = 2;

    static final int RARITY_NONE = 0;
    static final int RARITY_COMMON = 1;
    static final int RARITY_RARE = 2;
    static final int RARITY_VERYRARE = 3;

    static ModSelectItem[] MODS_LIST = new ModSelectItem[]{
            new ModSelectItem(R.drawable.empty, R.string.empty, ModSelectItem.MOD_NONE, ModSelectItem.RARITY_NONE),
            new ModSelectItem(R.drawable.hs_c, R.string.hs_c, ModSelectItem.MOD_HEATSINK, ModSelectItem.RARITY_COMMON),
            new ModSelectItem(R.drawable.hs_r, R.string.hs_r, ModSelectItem.MOD_HEATSINK, ModSelectItem.RARITY_RARE),
            new ModSelectItem(R.drawable.hs_vr, R.string.hs_vr, ModSelectItem.MOD_HEATSINK, ModSelectItem.RARITY_VERYRARE),
            new ModSelectItem(R.drawable.mh_c, R.string.mh_c, ModSelectItem.MOD_MULTIHACK, ModSelectItem.RARITY_COMMON),
            new ModSelectItem(R.drawable.mh_r, R.string.mh_r, ModSelectItem.MOD_MULTIHACK, ModSelectItem.RARITY_RARE),
            new ModSelectItem(R.drawable.mh_vr, R.string.mh_vr, ModSelectItem.MOD_MULTIHACK, ModSelectItem.RARITY_VERYRARE),
    };

    int img;
    int name;
    int rarity;
    int mod;

    ModSelectItem(int img, int name, int mod, int rarity) {
        this.img = img;
        this.name = name;
        this.mod = mod;
        this.rarity = rarity;
    }


}
