package ru.rsccdn.iwf.resistanceinteractivewatchface;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.wearable.activity.WearableActivity;
import android.support.wear.widget.WearableRecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import static ru.rsccdn.iwf.resistanceinteractivewatchface.ModSelectItem.MODS_LIST;

public class ModSelect extends WearableActivity {

    String MOD_SELECT_EXTRA = "MOD_SELECT_EXTRA";

    Typeface tf;

    public WearableRecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_mod_select);


        // Enables Always-on
        setAmbientEnabled();

        mRecyclerView = findViewById(R.id.recycler_mod_view);
//        mRecyclerView.setEdgeItemsCenteringEnabled(true);
        mRecyclerView.setLayoutManager(new android.support.wear.widget.WearableLinearLayoutManager(this));
        mRecyclerView.setAdapter(new ModSelectAdapter(MODS_LIST));

        tf = Typeface.createFromAsset(getAssets(), "exo2regular.ttf");
    }

    public class ModSelectAdapter extends WearableRecyclerView.Adapter<ModSelectAdapter.ModSelectViewHolder> {
        private ModSelectItem[] mDataset;

        class ModSelectViewHolder extends WearableRecyclerView.ViewHolder {
            ImageView mModImg;
            TextView mModName;

            ModSelectViewHolder(View v) {
                super(v);
                mModImg = v.findViewById(R.id.modImg);
                mModName = v.findViewById(R.id.modName);
            }

        }

        ModSelectAdapter(ModSelectItem[] myDataset) {
            mDataset = myDataset;
        }

        @NonNull
        @Override
        public ModSelectAdapter.ModSelectViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recycler_item, parent, false);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int itemPosition = mRecyclerView.getChildLayoutPosition(v);
                    ModSelectItem item = mDataset[itemPosition];
                    Intent result = new Intent();
                    result.putExtra(MOD_SELECT_EXTRA, itemPosition);
                    setResult(RESULT_OK, result);
                    finish();
                }
            });
            return new ModSelectViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ModSelectViewHolder holder, int position) {
            holder.mModImg.setImageResource(mDataset[position].img);
            holder.mModName.setText(getText(mDataset[position].name));
            holder.mModName.setTypeface(tf);
        }

        @Override
        public int getItemCount() {
            return mDataset.length;
        }
    }


}

