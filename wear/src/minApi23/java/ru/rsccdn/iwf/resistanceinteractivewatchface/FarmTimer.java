package ru.rsccdn.iwf.resistanceinteractivewatchface;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.SnapHelper;
import android.support.wear.widget.WearableRecyclerView;
import android.support.wearable.activity.WearableActivity;
import android.util.SparseIntArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Objects;

import static ru.rsccdn.iwf.resistanceinteractivewatchface.ModSelectItem.MODS_LIST;
import static ru.rsccdn.iwf.resistanceinteractivewatchface.ModSelectItem.MOD_HEATSINK;
import static ru.rsccdn.iwf.resistanceinteractivewatchface.ModSelectItem.MOD_MULTIHACK;
import static ru.rsccdn.iwf.resistanceinteractivewatchface.ModSelectItem.RARITY_VERYRARE;

public class FarmTimer extends WearableActivity {
    String MOD_SELECT_EXTRA = "MOD_SELECT_EXTRA";

    CountDownTimer timer;
    CountDownTimer cdTimer;

    Typeface tf;

    View settingsView;
    View timerView;
    int boTime = 14400;
    int cdTime = 300;
    int hackCount = 4;

    static SparseIntArray requestCodes;
    static {
        requestCodes = new SparseIntArray();
        requestCodes.put(R.id.modSlot1, 11);
        requestCodes.put(R.id.modSlot2, 12);
        requestCodes.put(R.id.modSlot3, 13);
        requestCodes.put(R.id.modSlot4, 14);
    }



    SharedPreferences prefs;

    public WearableRecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farm_timer);

        // Enables Always-on
        setAmbientEnabled();

        prefs = getPreferences(Context.MODE_PRIVATE);

        mRecyclerView = findViewById(R.id.recycler_pager_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new ViewsPagerAdapter());

        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(mRecyclerView);

        tf = Typeface.createFromAsset(getAssets(), "exo2regular.ttf");

    }

    public void drawCounters() {
        int[][] mods = getMods();

        float hcRatio = countHcRatio(mods);
        float cdRatio = countCdRatio(mods);

        long lastBoStart = prefs.getLong("lastBoStart", 0);
        long lastCdStart = prefs.getLong("lastCdStart", 0);
        int hackNumber = prefs.getInt("hackNumber", 0);

        long cd = ((long) (Math.round(cdTime * cdRatio)) + lastCdStart/1000) - (System.currentTimeMillis()/1000);
        if (lastCdStart == 0 || cd <= 0) {
            cd = (long) (Math.round(cdTime * cdRatio));
        }

        long bo = (boTime * 1000 + lastBoStart - System.currentTimeMillis())/1000;
        if (lastBoStart == 0 || bo <= 0) {
            bo = boTime;
        }

        int hc = Math.round(hackCount * hcRatio) - hackNumber;
        if (hc <= 0) {
            if (bo < boTime) {
                hc = 0;
            } else {
                hc = Math.round(hackCount * hcRatio);
            }
        }

        TextView vCd = timerView.findViewById(R.id.cdTimer);
        vCd.setText(formatCd(cd));
        TextView vBo = timerView.findViewById(R.id.boTimer);
        vBo.setText(formatBo(bo));
        TextView vHc = timerView.findViewById(R.id.hCount);
        vHc.setText(String.valueOf(hc));

        timerView.findViewById(R.id.layoutTimer).getBackground().setLevel((int) (100-(cd * 100 / (Math.round(cdTime * cdRatio))))*100);
        timerView.findViewById(R.id.layoutBo).getBackground().setLevel((int) (100-(bo * 100 / boTime)) * 100);
        timerView.findViewById(R.id.layoutHacks).getBackground().setLevel((int) (100 - (hc * 100 / (hackCount * hcRatio))) * 100);

    }

    private int[][] getMods() {
        int[][] mods = new int[3][4];
        int itemPos = prefs.getInt("slot_"+R.id.modSlot1, 0);
        ModSelectItem item = MODS_LIST[itemPos];
        mods[item.mod][item.rarity]++;

        itemPos = prefs.getInt("slot_"+R.id.modSlot2, 0);
        item = MODS_LIST[itemPos];
        mods[item.mod][item.rarity]++;

        itemPos = prefs.getInt("slot_"+R.id.modSlot3, 0);
        item = MODS_LIST[itemPos];
        mods[item.mod][item.rarity]++;

        itemPos = prefs.getInt("slot_"+R.id.modSlot4, 0);
        item = MODS_LIST[itemPos];
        mods[item.mod][item.rarity]++;

        return mods;
    }


    private String formatCd(long cd) {
        int mins = (int) cd / 60;
        int secs = (int) cd -  (60*mins);
        return String.format("%1s", mins).replace(' ', '0')+":"+String.format("%2s", secs).replace(' ', '0');
    }

    private String formatBo(long bo) {
        int hours = (int) bo / (60*60);
        int mins = (int) (bo - (60*60*hours)) / 60;
        int secs = (int) bo - (60*60*hours) - (60*mins);
        return String.format("%1s", hours).replace(' ', '0')+":"+String.format("%2s", mins).replace(' ', '0')+":"+String.format("%2s", secs).replace(' ', '0');
    }

    private float countHcRatio(int[][] mods) {
        float ratio = 1;
        int rarest = 0;
        for (int i = RARITY_VERYRARE; i > 0; i--) {
            int cnt = mods[MOD_MULTIHACK][i];
            if(cnt > 0 && rarest == 0) {
                rarest = i;
                ratio += i;
                cnt--;
            }
            ratio += (float) cnt * ((float)i / (float)2);
        }
        return ratio;
    }

    private float countCdRatio(int[][] mods) {
        float ratio = 1;
        int rarest = 0;
        float[] rarityRatios = {0, (float) 0.2, (float) 0.5, (float) 0.7};
        for (int i = RARITY_VERYRARE; i > 0; i--) {
            int cnt = mods[MOD_HEATSINK][i];
            if(cnt > 0) {
                if (rarest == 0) {
                    rarest = i;
                    ratio *= 1 - rarityRatios[i];
                    cnt--;
                }
                if (cnt > 0) {
                    for (int j = 1; j <= cnt; j++) {
                        ratio *= (1 - rarityRatios[i] / 2);
                    }
                }
            }
        }
        return ratio;
    }

    public void drawSlots() {
        drawSlot(R.id.modSlot1);
        drawSlot(R.id.modSlot2);
        drawSlot(R.id.modSlot3);
        drawSlot(R.id.modSlot4);
    }

    private void drawSlot(int slotId) {
        int itemPos = prefs.getInt("slot_"+slotId, 0);
        ModSelectItem item = MODS_LIST[itemPos];
        ImageButton v = settingsView.findViewById(slotId);
        v.setImageResource(item.img);
    }

    public void startTimer(long boTime, long cdTime) {
        timer = new CountDownTimer((long) (boTime * 1000), 1000) {
            public void onTick(long millisUntilFinished) {
                drawCounters();
            }

            public void onFinish() {
                drawCounters();
                Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(1000);
            }
        }.start();

        if (cdTime > 0) {
            startCdTimer(cdTime);
        }
    }

    private void startCdTimer(long cdTime) {
        cdTimer = new CountDownTimer(cdTime * 1000, 1000) {
            public void onTick(long millisUntilFinished) {
                drawCounters();
            }

            public void onFinish() {
                drawCounters();
                Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                vibe.vibrate(800);
            }

        }.start();
    }

    public class ViewsPagerAdapter extends WearableRecyclerView.Adapter<ViewsPagerAdapter.ViewsPagerViewHolder> {
        class ViewsPagerViewHolder extends WearableRecyclerView.ViewHolder {

            ViewsPagerViewHolder (View v) {
                super(v);
            }

        }

        class TimerViewHolder extends ViewsPagerViewHolder{
            public TimerViewHolder(View v){
                super(v);
                timerView = v;
                drawCounters();
                int[][] mods = getMods();
                float hcRatio = countHcRatio(mods);
                float cdRatio = countCdRatio(mods);
                long lastBoStart = prefs.getLong("lastBoStart", 0);
                long lastCdStart = prefs.getLong("lastCdStart", 0);
                int hackNumber = prefs.getInt("hackNumber", 0);

                long bo = (boTime * 1000 + lastBoStart - System.currentTimeMillis())/1000;
                long cd = ((long) (Math.round(cdTime * cdRatio)) + lastCdStart/1000) - (System.currentTimeMillis()/1000);
                if (lastCdStart == 0 || cd < 0) {
                    cd = 0;
                }
                if (lastBoStart != 0 && bo > 0) {
                    startTimer(bo, cd);
                }

                // Workaround for API < 25
                TextView vT = timerView.findViewById(R.id.watches);
                vT.setTypeface(tf);
                TextView vCd = timerView.findViewById(R.id.cdTimer);
                vCd.setTypeface(tf);
                TextView vHc = timerView.findViewById(R.id.hCount);
                vHc.setTypeface(tf);
                TextView vBoL = timerView.findViewById(R.id.burnoutLabel);
                vBoL.setTypeface(tf);
                TextView vBo = timerView.findViewById(R.id.boTimer);
                vBo.setTypeface(tf);


                final View.OnClickListener btnHandler = new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        int[][] mods = getMods();
                        float hcRatio = countHcRatio(mods);
                        float cdRatio = countCdRatio(mods);
                        long lastBoStart = prefs.getLong("lastBoStart", 0);
                        long lastCdStart = prefs.getLong("lastCdStart", 0);
                        int hackNumber = prefs.getInt("hackNumber", 0);

                        long bo = (boTime * 1000 + lastBoStart - System.currentTimeMillis())/1000;
                        long cd = ((long) (Math.round(cdTime * cdRatio)) + lastCdStart/1000) - (System.currentTimeMillis()/1000);
                        if ((hackNumber < hcRatio * hackCount || bo < 0) && (lastCdStart == 0 || cd <= 0)) {
                            if (lastBoStart == 0 || bo < 0) {
                                prefs.edit().putLong("lastBoStart", System.currentTimeMillis()).apply();
                                hackNumber = 0;
                                startTimer(boTime, (long) (Math.round(cdTime * cdRatio)));
                            }
                            hackNumber++;
                            prefs.edit().putInt("hackNumber", hackNumber).apply();
                            prefs.edit().putLong("lastCdStart", System.currentTimeMillis()).apply();


                            startCdTimer((long) (Math.round(cdTime * cdRatio)));
                        }

                    }

                };

                timerView.findViewById(R.id.hackBtn).setOnClickListener(btnHandler);

            }
        }

        class SettingsViewHolder extends ViewsPagerViewHolder {

            public SettingsViewHolder(View v){
                super(v);
                settingsView = v;
                final View.OnClickListener btnHandler = new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        int requestCode = requestCodes.get(v.getId());
                        if (requestCode > 10 && requestCode < 20) {
                            Intent list = new Intent(FarmTimer.this, ModSelect.class);
                            FarmTimer.this.startActivityForResult(list, requestCode);
                        }
                    }

                };

                settingsView.findViewById(R.id.modSlot1).setOnClickListener(btnHandler);
                settingsView.findViewById(R.id.modSlot2).setOnClickListener(btnHandler);
                settingsView.findViewById(R.id.modSlot3).setOnClickListener(btnHandler);
                settingsView.findViewById(R.id.modSlot4).setOnClickListener(btnHandler);

                drawSlots();
            }

        }


        @Override
        public int getItemViewType(int position) {
            // Just as an example, return 0 or 2 depending on position
            // Note that unlike in ListView adapters, types don't have to be contiguous
            return position;
        }

        @Override
        public ViewsPagerAdapter.ViewsPagerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v;
            switch (viewType) {
                case 1:
                    v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_farm_timer_settings, parent, false);
                    return new SettingsViewHolder(v);
                default:
                    v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_farm_timer_timer, parent, false);
                    return new TimerViewHolder(v);
            }
        }

        @Override
        public void onBindViewHolder(ViewsPagerViewHolder holder, int position) {
        }

        @Override
        public int getItemCount() {
            return 2;
        }

    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data) {
        if (requestCode > 10 && requestCode < 20) {
            if (resultCode == RESULT_OK) {
                int itemPos = Objects.requireNonNull(data.getExtras()).getInt(MOD_SELECT_EXTRA);
                int modSlotId = requestCodes.keyAt(requestCodes.indexOfValue(requestCode));
                prefs.edit().putInt("slot_"+modSlotId, itemPos).apply();
                if (MODS_LIST[itemPos].mod == MOD_HEATSINK) {
                    if (timer != null) {
                        timer.cancel();
                    }
                    if (cdTimer != null) {
                        cdTimer.cancel();
                    }
                    prefs.edit().putLong("lastBoStart", 0).apply();
                    prefs.edit().putInt("hackNumber", 0).apply();
                    prefs.edit().putLong("lastCdStart", 0).apply();
                }
                drawSlots();
                drawCounters();
            }
        }
    }


}
